# every data of this program has a tuple input (x0,x1) output and a color for output
class ColoredPoint:
    def __init__(self, x0, x1, y):
        self.x0 = x0
        self.x1 = x1
        self.y = y
        self.color = self.color_assign(y)

    @staticmethod
    def color_assign(y):
        if y == 1:
            return 'r'
        else:
            return 'b'

    @staticmethod
    def convert_to_point(list_colored_point):
        x = []
        y = []
        color = []
        for coloredPoint in list_colored_point:
            x.append(coloredPoint.x0)
            y.append(coloredPoint.x1)
            color.append(coloredPoint.color)
        return x, y, color
