import numpy as np
from colored_point import ColoredPoint


# Simple Neural Network
class UnaryNeuralNetwork:
    def __init__(self, n_epoch, lr,
                 number_train_records, number_total_records,
                 train, test):

        self.n_epoch = n_epoch
        self.lr = lr
        self.number_train_records = number_train_records
        self.number_total_records = number_total_records
        self.train = train
        self.test = test
        self.output = []
        self.W = None
        self.b = None
        self.accuracy = None

    def learning(self):
        # initialization 
        b = np.random.normal(0, 1, 1)
        W = np.random.normal(0, 1, 2)
        n_epoch = self.n_epoch
        lr = self.lr
        n = self.number_train_records
        for step in range(n_epoch):
            grad_W = np.array([0.0, 0.0])
            grad_b = 0
            for i in range(n):
                x0 = self.train[i].x0
                x1 = self.train[i].x1
                X = np.array([x0, x1])
                yT = self.train[i].y
                u = W.dot(X) + b
                y = self.sigmoid(u[0])
                grad_W += (y-yT) * X
                grad_b += (y-yT)

            # update parameters
            W = W - (lr * grad_W) / n
            b = b - (lr * grad_b) / n

        self.W = W
        self.b = b

    def testing(self):
        true_predict = 0
        W = self.W
        b = self.b
        for i in range(0, self.number_total_records - self.number_train_records):
            x0 = self.test[i].x0
            x1 = self.test[i].x1
            X = np.array([x0, x1])
            yT = self.test[i].y
            y = self.sigmoid(W.dot(X) + b)
            if y > 0.5:
                result = 1
            else:
                result = 0

            if result == yT:
                true_predict += 1

            self.output.append(ColoredPoint(self.test[i].x0, self.test[i].x1, result))
            self.accuracy = true_predict / (self.number_total_records - self.number_train_records)

    def sigmoid(self, x):
        return 1/(1+np.exp(-x))
