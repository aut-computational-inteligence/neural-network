import matplotlib.pyplot as plt
from colored_point import ColoredPoint
from unary_neural_network import UnaryNeuralNetwork
from complex_neural_network import ComplexNeuralNetwork

# Unary   : N_EPOCH = 100  , LEARNING_RATE = 30
# Complex : N_EPOCH = 20000, LEARNING_RATE = 2
N_EPOCH = 20000
LEARNING_RATE = 2
NUMBER_TRAIN_RECORDS = 100
NUMBER_TOTAL_RECORDS = 180
INPUT_FILE_NAME = "dataset.csv"

train = []
test = []
output = []


# read input data
def load_input_data():
    with open(INPUT_FILE_NAME) as f:
        lis = [line.split() for line in f]  # create a list of lists
        for i, row in enumerate(lis):  # print the list items
            if i > 0:
                x0, x1, y = row[0].split(",")
                new_x0 = float(x0)
                new_x1 = float(x1)
                if y == '1':
                    new_y = 1
                else:
                    new_y = 0
                if i <= NUMBER_TRAIN_RECORDS:
                    train.append(ColoredPoint(new_x0, new_x1, new_y))
                else:
                    test.append(ColoredPoint(new_x0, new_x1, new_y))


def plot_output_data():
    x, y, color = ColoredPoint.convert_to_point(output)
    plt.scatter(x=x, y=y, color=color)
    plt.title("Output Data")
    plt.show()


def plot_input_data():
    x, y, color = ColoredPoint.convert_to_point(train)
    plt.scatter(x=x, y=y, color=color)
    plt.title("Train Data")
    plt.show()

    x, y, color = ColoredPoint.convert_to_point(test)
    plt.scatter(x=x, y=y, color=color)
    plt.title("Test Data")
    plt.show()


load_input_data()
plot_input_data()
# neural_network = UnaryNeuralNetwork(N_EPOCH, LEARNING_RATE,
#                                     NUMBER_TRAIN_RECORDS, NUMBER_TOTAL_RECORDS,
#                                     train, test)

neural_network = ComplexNeuralNetwork(N_EPOCH, LEARNING_RATE,
                                      NUMBER_TRAIN_RECORDS, NUMBER_TOTAL_RECORDS,
                                      train, test)

# learning, testing, get output
neural_network.learning()
neural_network.testing()
output = neural_network.output

plot_output_data()
print("Accuracy : ", neural_network.accuracy)
