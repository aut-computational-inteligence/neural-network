import numpy as np
from colored_point import ColoredPoint


# Complex Neural Network
class ComplexNeuralNetwork:
    def __init__(self, n_epoch, lr,
                 number_train_records, number_total_records,
                 train, test):

        self.n_epoch = n_epoch
        self.lr = lr
        self.number_train_records = number_train_records
        self.number_total_records = number_total_records
        self.train = train
        self.test = test
        self.output = []
        self.W = None
        self.U = None
        self.V = None
        self.b0 = None
        self.b1 = None
        self.b2 = None
        self.accuracy = None

    def learning(self):
        # initialization of parameters
        b0 = np.random.normal(0, 1, 1)
        b1 = np.random.normal(0, 1, 1)
        b2 = np.random.normal(0, 1, 1)
        W = np.random.normal(0, 1, 2)
        V = np.random.normal(0, 1, 2)
        U = np.random.normal(0, 1, 2)
        n_epoch = self.n_epoch
        lr = self.lr
        n = self.number_train_records
        i = 0
        for step in range(n_epoch):
            # calculation like report
            x0 = self.train[i].x0
            x1 = self.train[i].x1
            X = np.array([x0, x1])
            yT = self.train[i].y
            p = X.dot(W) + b0
            q = X.dot(V) + b1
            z0 = self.sigmoid(p[0])
            z1 = self.sigmoid(q[0])
            Z = np.array([z0, z1])
            r = Z.dot(U) + b2
            y = self.sigmoid(r)
            u0 = U[0]
            u1 = U[1]

            # update parameters
            W = W - lr * (2 * (y - yT) * y * (1 - y) * u0 * z0 * (1 - z0) * X)
            V = V - lr * (2 * (y - yT) * y * (1 - y) * u1 * z1 * (1 - z1) * X)
            U = U - lr * (2 * (y - yT) * y * (1 - y) * Z)
            b0 = b0 - lr * (2 * (y - yT) * y * (1 - y) * u0 * z0 * (1 - z0))
            b1 = b1 - lr * (2 * (y - yT) * y * (1 - y) * u1 * z1 * (1 - z1))
            b2 = b2 - lr * (2 * (y - yT) * y * (1 - y))

            i += 1
            i %= n

        self.W = W
        self.U = U
        self.V = V
        self.b0 = b0
        self.b1 = b1
        self.b2 = b2

    def testing(self):
        true_predict = 0
        W = self.W
        U = self.U
        V = self.V
        b0 = self.b0
        b1 = self.b1
        b2 = self.b2
        for i in range(0, self.number_total_records - self.number_train_records):
            x0 = self.test[i].x0
            x1 = self.test[i].x1
            X = np.array([x0, x1])
            yT = self.test[i].y
            p = X.dot(W) + b0
            q = X.dot(V) + b1
            z0 = self.sigmoid(p[0])
            z1 = self.sigmoid(q[0])
            Z = np.array([z0, z1])
            r = Z.dot(U) + b2
            y = self.sigmoid(r)
            if y > 0.5:
                result = 1
            else:
                result = 0

            if result == yT:
                true_predict += 1

            # restore output and calculate accuracy
            self.output.append(ColoredPoint(self.test[i].x0, self.test[i].x1, result))
            self.accuracy = true_predict / (self.number_total_records - self.number_train_records)

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))
